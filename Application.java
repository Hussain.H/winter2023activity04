import java.util.Scanner;
public class Application{

	public static void main (String[] args){
		Scanner sc = new Scanner(System.in);
		
		//Constructor part
		
		Students coolkid = new Students("Abdul",99);
		System.out.println(coolkid.getName());
		System.out.println(coolkid.getGrade());
		System.out.println(coolkid.getenjoyClass());
		System.out.println(coolkid.getamountLearnt());
		
		System.out.println("Are you enjoying class?");
				//String ckenjoyClass= sc.nextLine();
				
				coolkid.setenjoyClass(("OF COURSE!"));
				System.out.println(coolkid.getenjoyClass());
		
			Students[] kid = new Students[4];
			
			for(int i =0; i < kid.length; i++){
				 kid[i] = new Students("Habib", 60);
				 
				 
				 System.out.println("Before using set Name method");
				 System.out.println(kid[i].getName());
				 
				System.out.println("What's your name?");
				String studentName = sc.nextLine();
				//kid[i].setName(studentName);
				
				System.out.println("After using set Name method");
				System.out.println(kid[i].getName());
				
				System.out.println("Are you enjoying class?");
				String studentenjoyClass= sc.nextLine();
				//kid[i].setenjoyClass(studentenjoyClass);
				
				System.out.println("Value after removing all setters "+kid[i].getenjoyClass());
				
				System.out.println("What is your average in programming? (0-100)");
				int studentGrade = sc.nextInt();
				//kid[i].setGrade(studentGrade);
				
				System.out.println("Value after removing all setters "+kid[i].getGrade());
				
				sc.nextLine();
				
			}
			
			System.out.println(kid[3].getName());
			System.out.println(kid[3].getenjoyClass());
			System.out.println(kid[3].getGrade());
			
			kid[0].stateName();
			kid[0].smart();
			
			System.out.println("Using get methods");
			System.out.println(kid[0].getName());
			System.out.println(kid[0].getGrade());
			System.out.println(kid[0].getenjoyClass());
			System.out.println(kid[0].getamountLearnt());
			//System.out.println(kid[0].amountLearnt);
			//System.out.println(kid[1].amountLearnt);
			//System.out.println(kid[2].amountLearnt);
			//kid[3].learn();
			//kid[3].learn();
			//System.out.println(kid[3].amountLearnt);
			System.out.println("Amountlearnt Before :");
			System.out.println(kid[0].getamountLearnt());
			System.out.println(kid[1].getamountLearnt());
			System.out.println(kid[2].getamountLearnt());
			System.out.println(kid[3].getamountLearnt());
			
			System.out.println("How many hours did u study? (numbers only)");
			int amountStudied = sc.nextInt();
			//kid[3].learn(amountStudied);
			System.out.println(kid[3].getamountLearnt());
			
			//kid[0].learn(5);
			//kid[1].learn(-5);
			
			System.out.println(kid[0].getamountLearnt());
			System.out.println(kid[1].getamountLearnt());
			System.out.println(kid[2].getamountLearnt());
			System.out.println(kid[3].getamountLearnt());
		}
}